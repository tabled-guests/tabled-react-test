import React, { useState } from 'react';
import './Celebrity/Celebrity.module.css';
import Celebrities from "./Celebrities";

function App() {
  const [celebritiesIds, setIds] = useState();

  const celebritiesUrl = 'https://halloffame-server.herokuapp.com/fames?guest=true'

  // fetch, retrieve and set celebrities' IDs on initial load

  return (
    <div>
      <h1>Celebrities</h1>
      {celebritiesIds && <Celebrities ids={celebritiesIds} />}
    </div>
  );
}

export default App;
