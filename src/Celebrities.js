import React, { useEffect, useState } from "react";
import Celebrity from "./Celebrity";

function Celebrities({ ids }) {
    const [celebrities, setCelebrities] = useState([]);
    const [currentIndex, setCurrentIndex] = useState(0);
    const [loading, setLoading] = useState(false);

    const fetchCelebrity = (id) => fetch(`https://halloffame-server.herokuapp.com/fames/${id}?guest=true`);

    const loadOne = () => {
        // fetch one celebrity
        // change current index
    };

    // load the first celebrity on initial load

    return (
        <div>
            <button disabled={undefined} onClick={undefined}>
                Load One More
            </button>
            {celebrities && celebrities.map((celebrity, index) => <Celebrity key={index} celebrity={celebrity} />)}
        </div>
    );
}

export default Celebrities;
